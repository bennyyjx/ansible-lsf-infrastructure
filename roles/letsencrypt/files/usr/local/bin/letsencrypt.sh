#!/bin/sh -e
#
# Let's Encrypt certificate generator
#
# Copyright (C) 2018-2019, 2021 Libre Space Foundation <https://libre.space/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

OPENSSL_KEY_DIR="/var/lib/letsencrypt"
OPENSSL_CERT_DIR="/var/lib/letsencrypt"
CHALLENGES_DIR="/var/lib/letsencrypt/challenges"
ACCOUNT_KEY_FILE="/var/lib/letsencrypt/account.key"
OPENSSL_CNF="/etc/pki/tls/openssl.cnf"
DAYS="31"

usage() {
	/bin/cat <<EOF
Usage: $(/bin/basename "$0") [OPTIONS]
Generate or renew a Let's Encrypt certificate

  -b                        Number of bits of the key
  -r                        Account email
  -n                        Name of certificate
  -c                        Configuration file
  --help                    Print usage

EOF
	exit 1
}

read_config() {
	if [ -f "$1" ]; then
		# shellcheck source=/dev/null
		. "$1"
		numbits="${LETSENCRYPT_NUMBITS:-4096}"
		account_email="$LETSENCRYPT_ACCOUNT_EMAIL"
		name="$LETSENCRYPT_NAME"
		domains="$LETSENCRYPT_DOMAINS"
	fi
}

parse_args() {
	while [ $# -gt 0 ]; do
		arg="$1"
		case $arg in
			-b)
                                shift
			        numbits=${1:-4096}
				;;
			-r)
			        shift
                                account_email="$1"
				if [ -z "$account_email" ]; then
				        usage
				fi
				;;
			-n)
			        shift
                                name="$1"
				if [ -z "$name" ]; then
				        usage
				fi
				;;
			-c)
				shift
				config_file="$1"
				if [ ! -f "$config_file" ]; then
					usage
				fi
				;;
			*)
			        domains="$domains $1"
				;;
		esac
		shift
	done
}

create_account_key() {
	if [ ! -f "$1" ]; then
		_umask="$(umask)"
		umask 0277
		/bin/openssl genrsa \
			-out "$1" \
			"$numbits"
		umask "$_umask"
	fi
}

create_san() {
	_domains="$*"

	for _domain in $_domains; do
		_san="${_san}${_san:+,}DNS:${_domain}"
	done
	echo "$_san"
}

create_key() {
	_name="$1"

	if [ ! -f "${OPENSSL_KEY_DIR}/${_name}.key" ]; then
		_umask="$(umask)"
		umask 0077
		/bin/openssl genrsa \
			-out "${OPENSSL_KEY_DIR}/${_name}.key" \
			"$numbits"
		umask "$_umask"
		/bin/rm -f "${OPENSSL_CERT_DIR}/${_name}.csr"
	fi
}

create_csr() {
	_name="$1"
	shift
	_domain="$1"
	_domains="$*"

	if [ ! -f "${OPENSSL_CERT_DIR}/${_name}.csr" ]; then
		if [ "$_domains" != "$_domain" ]; then
			_openssl_cnf="$(/bin/mktemp)"
			_san="$(create_san "$_domains")"

			/bin/cat "$OPENSSL_CNF" > "$_openssl_cnf"
			printf "[SAN]\nsubjectAltName=%s\n" "$_san" >> "$_openssl_cnf"
			/bin/openssl req \
				-new \
				-sha256 \
				-key "${OPENSSL_KEY_DIR}/${_name}.key" \
				-subj "/" \
				-reqexts SAN \
				-config "$_openssl_cnf" \
				-out "${OPENSSL_CERT_DIR}/${_name}.csr"
			/bin/rm "$_openssl_cnf"
		else
			/bin/openssl req \
				-new \
				-sha256 \
				-key "${OPENSSL_KEY_DIR}/${_name}.key" \
				-subj "/CN=${_domain}" \
				-out "${OPENSSL_CERT_DIR}/${_name}.csr"
		fi
	fi
}

check_crt() {
	_name="$1"

	_notafter=$(/bin/openssl x509 \
			    -enddate \
			    -noout \
			    -in "${OPENSSL_CERT_DIR}/${_name}.crt" \
			    | /bin/awk 'BEGIN { FS="=" } /^notAfter=/ { print $2 }')
	if [ "$((($(/bin/date --date="$_notafter" "+%s") - $(/bin/date "+%s")) / 86400))" -lt "$DAYS" ]; then
		return 1
	fi

	return 0
}

get_crt() {
	_name="$1"

	_temp_crt="$(mktemp)"
	/sbin/acme-tiny \
		--contact "mailto:$account_email" \
		--account-key "$ACCOUNT_KEY_FILE" \
		--csr "${OPENSSL_CERT_DIR}/${_name}.csr" \
		--acme-dir "$CHALLENGES_DIR" > "$_temp_crt"
	cat "$_temp_crt" > "${OPENSSL_CERT_DIR}/${_name}.crt"
	rm "$_temp_crt"
}

parse_args "$@"
read_config "$config_file"
if [ -z "$domains" ] || [ -z "$name" ] || [ -z "$account_email" ]; then
        usage
fi
/bin/mkdir -p "$OPENSSL_KEY_DIR" "$OPENSSL_CERT_DIR" "$CHALLENGES_DIR"
create_account_key "$ACCOUNT_KEY_FILE"
create_key "$name"
# shellcheck disable=SC2086
create_csr "$name" $domains
if ! check_crt "$name"; then
	get_crt "$name"
fi
